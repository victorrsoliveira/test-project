# GitLab Merge Request - Manual

## Roles

1. Assignee
2. Reviewer

## Discussion

1. Highlight changes and comment on them
   1. Add to Review and Submit Review
2. Threads start when replying a comment
   1. They can be resolvable
3. Suggest changes by editing the code directly in the MR
4. Replying comments
   1. Start a review
      1. This starts a review, so you can add many comments and wen you're ready, you publish them all in a single action.
   2. Add comment now
      1. Comment is added immediately.
5. In the Changes tab you can click on a specific line of code and get the address.
   1. This address can be used as a reference in the comment in the MR page.
6. You can reference commits in the comments as well. Just copy the hash and use it in the comments.
7. You can mark the file as "Viewed" in the Changes tab.
8. You can view pipeline results in the MR page.

## Issues

1. Create issues from comments or threads.

## Drafts

## Update MR

1. When someone pushes to a branch that is tied to a MR, these changes are added to the MR and new versions are created for this MR.
2. HEAD comparison
3. Versions are created when new commits are added to the branch with an MR open

## Resolving conflicts

## Revert MRs